FROM debian:10-slim

ENV DEBIAN_FRONTEND=noninteractive \
    LANG=C.UTF-8 \
    LANGUAGE=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    LC_TYPE=C.UTF-8

# Clean docs
RUN echo "path-exclude /usr/share/doc/*" > /etc/dpkg/dpkg.cfg.d/01_nodoc && \
    echo "path-include /usr/share/doc/*/copyright" >> /etc/dpkg/dpkg.cfg.d/01_nodoc && \
    echo "path-exclude /usr/share/man/*" >> /etc/dpkg/dpkg.cfg.d/01_nodoc && \
    echo "path-exclude /usr/share/groff/*" >> /etc/dpkg/dpkg.cfg.d/01_nodoc && \
    echo "path-exclude /usr/share/info/*" >> /etc/dpkg/dpkg.cfg.d/01_nodoc && \
    echo "path-exclude /usr/share/lintian/*" >> /etc/dpkg/dpkg.cfg.d/01_nodoc && \
    echo "path-exclude /usr/share/linda/*" >> /etc/dpkg/dpkg.cfg.d/01_nodoc  && \
    find /usr/share/doc -depth -type f ! -name copyright|xargs rm || true && \
    find /usr/share/doc -empty|xargs rmdir || true && \
    rm -rf /usr/share/man/* /usr/share/groff/* /usr/share/info/* && \
    rm -rf /usr/share/lintian/* /usr/share/linda/* /var/cache/man/*

WORKDIR /tmp

RUN apt-get -y update && \
    apt-get -y install \
      bash \
      bash-completion \
      net-tools \
      inetutils-ping \
      git \
      wget \
      vim \
      unzip \
      git-crypt \
      curl \
      gnupg2 && \
    apt-get -y update && \
    apt-get -y purge aptitude g++ libssl-dev gcc libc-dev && \
    apt-get -y autoremove && apt-get -y clean

ENV YAML2JSON_VERSION=1.3 \
    YAML2JSON_SHA256=e792647dd757c974351ea4ad35030852af97ef9bbbfb9594f0c94317e6738e55 \
    YQ_VERSION=2.3.0 \
    YQ_SHA256=97b2c61ae843a429ce7e5a2c470cfeea1c9e9bf317793b41983ef228216fe31b \
    TERRAFORM_VERSION=0.13.4 \
    TERRAFORM_SHA256=a92df4a151d390144040de5d18351301e597d3fae3679a814ea57554f6aa9b24 \
    TERRAGRUNT_VERSION=0.24.1 \
    TERRAGRUNT_SHA256=3f83d225d1335dc02a20fa604ee3ea7f14efcd68e952b902652b7287eed8c18b \
    #TERRAFORM_SNOWFLAKE_PROVIDER_VERSION=0.13.0 \
    #TERRAFORM_SNOWFLAKE_PROVIDER_SHA256=ee19140b84e177acab260dce7beabce2cf8e3827a4e1df6d75a736e6d137a9be \
    JQ_VERSION=1.5 \
    JQ_SHA256=c6b3a7d7d3e7b70c6f51b706a3b90bd01833846c54d32ca32f0027f00226ff6d

#yaml2json
RUN set -e \
    && wget -q -O /usr/local/bin/yaml2json https://github.com/bronze1man/yaml2json/releases/download/v${YAML2JSON_VERSION}/yaml2json_linux_amd64 \
    && chmod +x /usr/local/bin/yaml2json \
    && cd /usr/local/bin \
    && echo "$YAML2JSON_SHA256  yaml2json" | sha256sum -c \
#yq
    && wget -q -O /usr/local/bin/yq https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64 \
    && chmod +x /usr/local/bin/yq \
    && cd /usr/local/bin \
    && echo "$YQ_SHA256  yq" | sha256sum -c \
# terraform
    && cd /tmp \
    && wget -q https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -O /tmp/terraform.zip \
    && echo "$TERRAFORM_SHA256  terraform.zip" | sha256sum -c \
    && unzip terraform.zip -d /usr/local/bin \
    && chmod +x /usr/local/bin/terraform \
    && rm -f terraform.zip \
# terragrunt
    && wget -q https://github.com/gruntwork-io/terragrunt/releases/download/v${TERRAGRUNT_VERSION}/terragrunt_linux_amd64 -O /tmp/terragrunt \
    && echo "$TERRAGRUNT_SHA256  terragrunt" | sha256sum -c \
    && mv /tmp/terragrunt /usr/local/bin \
    && chmod +x /usr/local/bin/terragrunt \

## JQ
    && wget -q https://github.com/stedolan/jq/releases/download/jq-${JQ_VERSION}/jq-linux64 -O /tmp/jq \
    && chmod +x /tmp/jq \
    && echo "$JQ_SHA256  jq" | sha256sum -c \
    && mv /tmp/jq /usr/local/bin

USER 0

RUN echo 'source /usr/share/google-cloud-sdk/completion.bash.inc' >> /etc/profile && \
    echo "mkdir /tmp/home" >> /etc/profile 

RUN useradd -ms /bin/bash mintel
USER mintel

RUN echo "eval \`ssh-agent\`" >> /home/mintel/.bashrc && \
    echo "ssh-add /home/mintel/.ssh/id_rsa" >> /home/mintel/.bashrc
    
# Extend PATH for mintel user
RUN echo 'PATH=$HOME/.local/bin:$PATH' >> /home/mintel/.bashrc && \
    echo 'export PS1="\[\033[95m\]\u@\h \[\033[32m\]\w\[\033[33m\] [\$(git symbolic-ref --short HEAD 2>/dev/null)]\[\033[00m\]\$ "' >> /home/mintel/.bashrc

ENV PATH=/home/mintel/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin \
    DOCKER_HOST_ALIAS=docker \
    KIND_NODES=1 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8 \
    LC_TYPE=en_US.UTF-8

# Don't use a real entrypoint
ENTRYPOINT ["/usr/bin/env"]
