This repository has enabled Gitlab Runner

registry.gitlab.com/mfigura/tf-snowfake-docker

Need to attach volumes:
- aws credentials for S3 backend with TF state files
- ssh key so you can acces git project from the container
- git project for terragrunt-snowfake-services*
- 
docker pull registry.gitlab.com/mfigura/tf-snowfake-docker && docker run -it --rm -v /home/mfigura/git/terragrunt-snowflake-services-minteltest/:/home/mintel/terragrunt-snowflake-services-mintel-test -v /home/mfigura/.aws/:/home/mintel/.aws -v /home/mfigura/key/:/home/mintel/.ssh registry.gitlab.com/mfigura/tf-snowfake-docker bash

You can add this to your .bashrc file as an alias.
